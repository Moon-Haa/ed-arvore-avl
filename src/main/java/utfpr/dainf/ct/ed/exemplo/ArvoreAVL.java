package utfpr.dainf.ct.ed.exemplo;

import java.util.Stack;
import static utfpr.dainf.ct.ed.exemplo.ArvoreBinaria.calculaAltura;


public class ArvoreAVL<E extends Comparable<E>> extends ArvoreBinariaPesquisa<E> {

    protected byte fb;

    public ArvoreAVL() {
    }

    public ArvoreAVL(E valor) {
        super(valor);
    }

    public byte getFb() {
        return fb;
    }

    protected void setFb(byte fb) {
        this.fb = fb;
    }

    public ArvoreBinariaPesquisa<E> Pesquisa(E valor)
    {

        if (valor.compareTo(this.valor) == 0) {
            return this;
        } else if (valor.compareTo(this.valor) < 0) {
            if (this.esquerda == null) {
                return null;
            } else {
                return ((ArvoreBinariaPesquisa<E>) this.esquerda).pesquisa(valor);
            }
        } else {
            if (this.direita == null) {
                return null;
            } else {
                return ((ArvoreBinariaPesquisa<E>) this.direita).pesquisa(valor);
            }
        }
    }

    protected ArvoreAVL<E> rotacaoEsquerda(ArvoreAVL<E> x) {
        ArvoreAVL<E> root = (ArvoreAVL<E>) getRoot();
        ArvoreAVL<E> y = (ArvoreAVL) x.direita;

        x.setDireita(y.esquerda);
        y.setPai(x.pai);

        if (x.pai == null) {
            root = y;
        } else {
            if (x == x.pai.esquerda) {
                x.pai.setEsquerda(y);
            } else {
                x.pai.setDireita(y);
            }
        }

        y.setEsquerda(x);

        x.setPai(y);

        return root;
    }

    protected ArvoreAVL<E> rotacaoDireita(ArvoreAVL<E> y) {
        ArvoreAVL<E> root = (ArvoreAVL<E>) getRoot();
        ArvoreAVL<E> x = (ArvoreAVL) y.esquerda;

        y.setEsquerda(x.direita);
        x.setPai(y.pai);

        if (y.pai == null) {
            root = x;
        } else {
            if (y == y.pai.esquerda) {
                y.pai.setEsquerda(x);
            } else {
                y.pai.setDireita(x);
            }
        }

        x.setDireita(y);

        y.setPai(x);

        return root;
    }

    protected ArvoreAVL<E> rotacao(ArvoreAVL<E> p, ArvoreAVL<E> q) {
        ArvoreAVL<E> root = (ArvoreAVL<E>) getRoot();
        ArvoreAVL<E> u = q;

        while (u.pai != p && u.pai != null) {
            u = (ArvoreAVL) u.pai;
        }

        if (p.fb < 0) {
            if (u.fb < 0) {
                root = root.rotacaoDireita(p);
            } else {
                root = root.rotacaoEsquerda((ArvoreAVL) p.esquerda);
                root = root.rotacaoDireita(p);
            }
        } else {
            if (u.fb > 0) {
                root = root.rotacaoEsquerda(p);
            } else {
                root = root.rotacaoDireita((ArvoreAVL) p.direita);
                root = root.rotacaoEsquerda(p);
            }
        }

        return root;
    }

    protected ArvoreAVL<E> ajustaFbInsercao(ArvoreAVL<E> no) {
        ArvoreAVL<E> temp = no;
        while (temp != null) {
            int a = calculaAltura(temp.esquerda);
            int b = calculaAltura(temp.direita);
            temp.setFb((byte) (b - a));
            temp = (ArvoreAVL) temp.pai;
        }

        return this;
    }

    protected ArvoreAVL<E> ajustaFbExclusao(ArvoreAVL<E> no) {
        ArvoreAVL<E> temp = (ArvoreAVL<E>) getMinimo();

        while (temp != null) {
            temp.fb = (byte) (calculaAltura(temp.direita) - calculaAltura(temp.esquerda));
            temp = (ArvoreAVL<E>) sucessor(temp);
        }

        return this;
    }

    protected ArvoreAVL<E> insere(ArvoreAVL<E> no) {
        super.insere(no);

        ArvoreAVL<E> temp = no;
        ajustaFbInsercao(no);
        int tem = 0;
        while (temp != null) {
            if (Math.abs(temp.fb) > 1) {
                tem = 1;
                break;
            }
            temp = (ArvoreAVL) temp.pai;
        }

        if (tem == 1) {
            rotacao(temp, no);
            ajustaFbExclusao(no);
        }

        return (ArvoreAVL<E>)getRoot();
    }

    public ArvoreAVL<E> insere(E valor) {
        return insere(new ArvoreAVL<>(valor));
    }

    public ArvoreAVL<E> exclui(ArvoreAVL<E> no) {
        ArvoreAVL<E> root = (ArvoreAVL<E>) getRoot();

        no = (ArvoreAVL) root.pesquisa(no.valor);

        if (root == no && (root.esquerda == null || root.direita == null)) {
            if (root.esquerda != null) {
                ((ArvoreAVL) root.esquerda).pai = null;
                return (ArvoreAVL) root.esquerda;
            } else {
                if (root.direita != null) {
                    ((ArvoreAVL) root.direita).pai = null;
                }
                return (ArvoreAVL) root.direita;
            }
        }

        if (no.esquerda != null && no.direita != null) {
            ArvoreAVL<E> sucessorValido = no;
            sucessorValido = (ArvoreAVL) sucessor(sucessorValido);
            E valor = sucessorValido.valor;
            exclui(sucessorValido);
            no.valor = valor;

        } else {

            boolean isFilhoDaEsquerda = (no.pai.esquerda == no);
            boolean isTemFilhoEsquerda = (no.esquerda != null);

            if (no.esquerda == null && no.direita == null) {
                if (isFilhoDaEsquerda) {
                    no.pai.esquerda = null;
                } else {
                    no.pai.direita = null;
                }
	    }
            else {
                if (isFilhoDaEsquerda) {
                    if (isTemFilhoEsquerda) {
                        no.pai.esquerda = no.esquerda;
                        ((ArvoreAVL<E>) no.esquerda).pai = no.pai;
                    } else {
                        no.pai.esquerda = no.direita;
                        ((ArvoreAVL<E>) no.direita).pai = no.pai;
                    }
                } else {
                    if (isTemFilhoEsquerda) {
                        no.pai.direita = no.esquerda;
                        ((ArvoreAVL<E>) no.esquerda).pai = no.pai;
                    } else {
                        no.pai.direita = no.direita;
                        ((ArvoreAVL<E>) no.direita).pai = no.pai;
                    }
                }
            }

            no = (ArvoreAVL) no.pai;
            int tempAltura;
            while (no != null) {
                tempAltura = (calculaAltura(no.direita) - calculaAltura(no.esquerda));

                if (Math.abs(tempAltura) > 1) {
                    if (tempAltura < 0) {
                        root = root.rotacaoDireita(no);
                    } else {
                        root = root.rotacaoEsquerda(no);
                    }
                }

                no = (ArvoreAVL) no.pai;
            }

            ajustaFbExclusao(no);
        }

        return root;
    }

}
