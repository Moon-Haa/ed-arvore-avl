package utfpr.dainf.ct.ed.exemplo;

import java.util.ArrayDeque;
import java.util.Stack;


public class ArvoreBinaria<E> {

    protected E valor;
    protected ArvoreBinaria<E> esquerda;
    protected ArvoreBinaria<E> direita;

    private boolean inicio = true;
    private Stack<ArvoreBinaria<E>> pilha;
    private ArrayDeque<ArvoreBinaria<E>> fila;
    private ArvoreBinaria<E> ultimoVisitado;
    private ArvoreBinaria<E> noPos;

    public static int calculaAltura(ArvoreBinaria no) {
        int alturaEsquerda, alturaDireita;
        if (no == null) {
            return -1;
        }

        alturaEsquerda = calculaAltura(no.esquerda);
        alturaDireita = calculaAltura(no.direita);

        if (alturaEsquerda > alturaDireita) {
            return alturaEsquerda + 1;
        } else {
            return alturaDireita + 1;
        }
    }

    public ArvoreBinaria() {
    }

    public ArvoreBinaria(E valor) {
        this.valor = valor;
    }

    public ArvoreBinaria<E> insereEsquerda(ArvoreBinaria<E> a) {
        ArvoreBinaria<E> e = esquerda;
        ArvoreBinaria<E> x = a;
        esquerda = a;
        while (x.esquerda != null) {
            x = x.esquerda;
        }
        x.esquerda = e;
        return a;
    }

    public ArvoreBinaria<E> insereDireita(ArvoreBinaria<E> a) {
        ArvoreBinaria<E> d = direita;
        ArvoreBinaria<E> x = a;
        direita = a;
        while (x.direita != null) {
            x = x.direita;
        }
        x.direita = d;
        return a;
    }

    protected void visita(ArvoreBinaria<E> no) {
        System.out.print(" " + no.valor);
    }

    public void visitaEmOrdem(ArvoreBinaria<E> raiz) {
        if (raiz != null) {
            visitaEmOrdem(raiz.esquerda);
            visita(raiz);
            visitaEmOrdem(raiz.direita);
        }
    }

    public void visitaEmOrdem() {
        visitaEmOrdem(this);
    }

    public void visitaPreOrdem(ArvoreBinaria<E> raiz) {
        if (raiz != null) {
            visita(raiz);
            visitaPreOrdem(raiz.esquerda);
            visitaPreOrdem(raiz.direita);
        }
    }

    public void imprimePreOrdem(ArvoreBinaria<E> raiz, int nivel) {
        if (raiz != null) {
            for(int i=0;i<nivel;i++){
                System.out.print('*');
            }
            System.out.println(raiz.valor);
            imprimePreOrdem(raiz.esquerda,nivel+1);
            imprimePreOrdem(raiz.direita,nivel+1);
        }
    }

    public void visitaPreOrdem() {
        visitaPreOrdem(this);
    }

    public void visitaPosOrdem(ArvoreBinaria<E> raiz) {
        if (raiz != null) {
            visitaPosOrdem(raiz.esquerda);
            visitaPosOrdem(raiz.direita);
            visita(raiz);
        }
    }

    public void visitaPosOrdem() {
        visitaPosOrdem(this);
    }

    private void inicializaPilha() {
        if (pilha == null) {
            pilha = new Stack<>();
        }
    }

    private void inicializaFila() {
        if (fila == null) {
            fila = new ArrayDeque<>();
        }
    }

    public void reinicia() {
        inicializaPilha();
        inicializaFila();
        pilha.clear();
        fila.clear();
        ultimoVisitado = this;
        inicio = true;
    }

    public ArvoreBinaria<E> proximoEmOrdem() {
        ArvoreBinaria<E> resultado = null;
        if (inicio) {
            reinicia();
            inicio = false;
        }
        if (!pilha.isEmpty() || ultimoVisitado != null) {
            while (ultimoVisitado != null) {
                pilha.push(ultimoVisitado);
                ultimoVisitado = ultimoVisitado.esquerda;
            }
            ultimoVisitado = pilha.pop();
            resultado = ultimoVisitado;
            ultimoVisitado = ultimoVisitado.direita;
        }
        return resultado;
    }

    public ArvoreBinaria<E> proximoPreOrdem() {
        ArvoreBinaria<E> resultado = null;

        if (inicio) {
            reinicia();
            inicio = false;
        }

        if (ultimoVisitado != null) {
            pilha.push(ultimoVisitado);
        }
        if (!pilha.isEmpty()) {
            ultimoVisitado = pilha.pop();
            resultado = ultimoVisitado;
            if (ultimoVisitado != null) {
                if (ultimoVisitado.direita != null) {
                    pilha.push(ultimoVisitado.direita);
                }
                if (ultimoVisitado.esquerda != null) {
                    pilha.push(ultimoVisitado.esquerda);
                }
            }
            ultimoVisitado = !pilha.isEmpty() ? pilha.pop() : null;
        }
        return resultado;
    }

    public ArvoreBinaria<E> proximoPosOrdem() {
        ArvoreBinaria<E> resultado = null;
        boolean primeira = inicio;
        if (inicio) {
            reinicia();
            inicio = false;
        }

        if (pilha.isEmpty() && !primeira) {
            return null;
        } else {
            resultado = pilha.isEmpty() ? ultimoVisitado : pilha.pop();
        }

        while (true) {
            if (resultado.esquerda != null
                    || resultado.direita != null) {

                if (resultado.esquerda != null
                        && resultado.esquerda != ultimoVisitado
                        && resultado.direita != ultimoVisitado) {
                    pilha.push(resultado);
                    resultado = resultado.esquerda;
                } else if (resultado.direita != null
                        && resultado.direita != ultimoVisitado) {
                    pilha.push(resultado);
                    resultado = resultado.direita;
                } else {
                    ultimoVisitado = resultado;
                    return resultado;
                }
            } else {
                ultimoVisitado = resultado;
                return resultado;
            }
        }
    }

    public ArvoreBinaria<E> proximoEmNivel() {
        ArvoreBinaria<E> resultado = null;
        if (inicio) {
            reinicia();
            inicio = false;
            fila.add(this);
        }

        if (!fila.isEmpty()) {

            resultado = fila.poll();

            if (resultado.esquerda != null) {
                fila.add(resultado.esquerda);
            }
            if (resultado.direita != null) {
                fila.add(resultado.direita);
            }
        }
        return resultado;
    }

    public E getValor() {
        return valor;
    }

    public void setValor(E valor) {
        this.valor = valor;
    }

    protected ArvoreBinaria<E> getEsquerda() {
        return esquerda;
    }

    protected ArvoreBinaria<E> getDireita() {
        return direita;
    }

    protected void setEsquerda(ArvoreBinaria<E> esquerda) {
        this.esquerda = esquerda;
    }

    protected void setDireita(ArvoreBinaria<E> direita) {
        this.direita = direita;
    }

}

