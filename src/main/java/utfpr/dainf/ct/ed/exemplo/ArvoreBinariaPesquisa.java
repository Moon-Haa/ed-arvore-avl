package utfpr.dainf.ct.ed.exemplo;

import java.util.ArrayDeque;
import java.util.Stack;


public class ArvoreBinariaPesquisa<E extends Comparable<E>> extends ArvoreBinaria<E> {

    public ArvoreBinariaPesquisa<E> pai;
    protected Stack<ArvoreBinariaPesquisa<E>> pilha;
    protected ArvoreBinariaPesquisa<E> ultimoVisitado;

    private boolean inicio = true;
    private ArvoreBinariaPesquisa<E> noPos;

    private void inicializaPilha() {
        if (pilha == null) {
            pilha = new Stack<>();
        }
    }

    public void reinicia() {
        inicializaPilha();
        pilha.clear();
        ultimoVisitado = this;
        inicio = true;
    }

    protected ArvoreBinaria<E> getEsquerda() {
        return esquerda;
    }

    protected ArvoreBinaria<E> getDireita() {
        return direita;
    }

    protected void setEsquerda(ArvoreBinariaPesquisa<E> esquerda) {
        this.esquerda = esquerda;
    }

    protected void setDireita(ArvoreBinariaPesquisa<E> direita) {
        this.direita = direita;
    }

    public ArvoreBinariaPesquisa<E> getRoot() {
        ArvoreBinariaPesquisa<E> root = this;

        while (root.pai != null) {
            root = root.pai;
        }
        return root;
    }

    public ArvoreBinariaPesquisa<E> insereEsquerda(ArvoreBinariaPesquisa<E> a) {
        ArvoreBinariaPesquisa<E> e = (ArvoreBinariaPesquisa<E>) esquerda;
        ArvoreBinariaPesquisa<E> x = a;
        esquerda = a;
        while (x.esquerda != null) {
            x = (ArvoreBinariaPesquisa<E>) x.esquerda;
        }
        x.esquerda = e;
        return a;
    }

    public ArvoreBinariaPesquisa<E> insereDireita(ArvoreBinariaPesquisa<E> a) {
        ArvoreBinariaPesquisa<E> d = (ArvoreBinariaPesquisa<E>) direita;
        ArvoreBinariaPesquisa<E> x = a;
        direita = a;
        while (x.direita != null) {
            x = (ArvoreBinariaPesquisa<E>) x.direita;
        }
        x.direita = d;
        return a;
    }

    public void visitaEmOrdem(ArvoreBinariaPesquisa<E> raiz) {
        if (raiz != null) {
            visitaEmOrdem(raiz.esquerda);
            visita(raiz);
            visitaEmOrdem(raiz.direita);
        }
    }

    public ArvoreBinariaPesquisa<E> proximoEmOrdem() {
        ArvoreBinariaPesquisa<E> resultado = null;
        if (inicio) {
            reinicia();
            inicio = false;
        }
        if (!pilha.isEmpty() || ultimoVisitado != null) {
            while (ultimoVisitado != null) {
                pilha.push(ultimoVisitado);
                ultimoVisitado = (ArvoreBinariaPesquisa<E>) ultimoVisitado.esquerda;
            }
            ultimoVisitado = pilha.pop();
            resultado = ultimoVisitado;
            ultimoVisitado = (ArvoreBinariaPesquisa<E>) ultimoVisitado.direita;
        }
        return resultado;
    }

    public void visitaEmOrdem() {
        visitaEmOrdem(this);
    }

    public ArvoreBinariaPesquisa() {
    }

    public ArvoreBinariaPesquisa(E valor) {
        super(valor);
    }

    protected void setPai(ArvoreBinariaPesquisa<E> pai) {
        this.pai = pai;
    }

    public ArvoreBinariaPesquisa<E> getPai() {
        return pai;
    }

    public ArvoreBinariaPesquisa<E> pesquisa(E valor) {
        if (valor.compareTo(this.valor) == 0) {
            return this;
        } else if (valor.compareTo(this.valor) < 0) {
            if (this.esquerda == null) {
                return null;
            } else {
                return ((ArvoreBinariaPesquisa<E>) this.esquerda).pesquisa(valor);
            }
        } else {
            if (this.direita == null) {
                return null;
            } else {
                return ((ArvoreBinariaPesquisa<E>) this.direita).pesquisa(valor);
            }
        }
    }

    public ArvoreBinariaPesquisa<E> getMinimoRamo()
    {
        ArvoreBinariaPesquisa<E> resultado = this;
        while (resultado.esquerda != null) {
            resultado = (ArvoreBinariaPesquisa) resultado.esquerda;
        }
        return resultado;
    }

    public ArvoreBinariaPesquisa<E> getMinimo()
    {
        ArvoreBinariaPesquisa<E> root = getRoot();

        ArvoreBinariaPesquisa<E> resultado = root;
        while (resultado.esquerda != null) {
            resultado = (ArvoreBinariaPesquisa) resultado.esquerda;
        }
        return resultado;
    }

    public ArvoreBinariaPesquisa<E> getMaximoRamo(){
        ArvoreBinariaPesquisa<E> resultado = this;
        while (resultado.direita != null) {
            resultado = (ArvoreBinariaPesquisa) resultado.direita;
        }
        return resultado;
    }

    public ArvoreBinariaPesquisa<E> getMaximo() {
        ArvoreBinariaPesquisa<E> root = getRoot();
        ArvoreBinariaPesquisa<E> resultado = root;
        while (resultado.direita != null) {
            resultado = (ArvoreBinariaPesquisa) resultado.direita;
        }
        return resultado;
    }

    public ArvoreBinariaPesquisa<E> sucessor(ArvoreBinariaPesquisa<E> no) {
        ArvoreBinariaPesquisa<E> root = getRoot();
        no = root.pesquisa(no.valor);
        if(no==null){
            return null;
        }
        if (no.direita != null) {
            return ((ArvoreBinariaPesquisa<E>) no.direita).getMinimoRamo();
        }

        ArvoreBinariaPesquisa<E> pai = no.pai;

        while (pai != null && no.equals(pai.direita)) {
            no = pai;
            pai = pai.pai;
        }

        return pai;
    }

    public ArvoreBinariaPesquisa<E> predecessor(ArvoreBinariaPesquisa<E> no) {
        ArvoreBinariaPesquisa<E> root = getRoot();
        no = root.pesquisa(no.valor);
        if (no != null && no.esquerda != null) {
            return ((ArvoreBinariaPesquisa<E>) no.esquerda).getMaximoRamo();
        }

        ArvoreBinariaPesquisa<E> pai = no.pai;

        while (pai != null && no.equals(pai.esquerda)) {
            no = pai;
            pai = pai.pai;
        }
        return pai;
    }

    public ArvoreBinariaPesquisa<E> insere(E valor) {
        return insere(new ArvoreBinariaPesquisa<>(valor));
    }

    public ArvoreBinariaPesquisa<E> insere(ArvoreBinariaPesquisa<E> no) {
        ArvoreBinariaPesquisa<E> root = getRoot();
        return root.inserir(no);
    }
    public ArvoreBinariaPesquisa<E> inserir(ArvoreBinariaPesquisa<E> no) {
        if (no.valor.compareTo(this.valor) < 0) {
            if (this.esquerda == null) {

                this.setEsquerda(no);
                no.setPai(this);
                return no;
            } else {
                return ((ArvoreBinariaPesquisa<E>) this.esquerda).inserir(no);
            }
        } else {
            if (this.direita == null) {

                this.setDireita(no);
                no.setPai(this);
                return no;
            } else {
                return ((ArvoreBinariaPesquisa<E>) this.direita).inserir(no);
            }
        }
    }

    public ArvoreBinariaPesquisa<E> exclui(ArvoreBinariaPesquisa<E> no) {
        no = pesquisa(no.valor);
        ArvoreBinariaPesquisa<E> resultado = no;

        if (resultado.direita == null && resultado.esquerda == null) {
            if (resultado.pai == null)
            {
                no.valor = null;
            } else {
                if (resultado == resultado.pai.direita) {
                    resultado.pai.direita = null;
                } else if (resultado == resultado.pai.esquerda) {
                    resultado.pai.esquerda = null;
                }
            }
        }
        else if (resultado.direita != null && resultado.esquerda == null)
        {
            if (resultado.pai != null)
            {
                ((ArvoreBinariaPesquisa<E>) (resultado.direita)).setPai(resultado.pai);
                if (resultado == resultado.pai.direita) {
                    resultado.pai.direita = resultado.direita;
                } else if (resultado == resultado.pai.esquerda) {
                    resultado.pai.esquerda = resultado.direita;
                }
            } else
            {
                resultado.valor = resultado.direita.valor;
                resultado.direita = null;
            }
        } else if (resultado.direita == null && resultado.esquerda != null)
        {
            if (resultado.pai != null) {
                ((ArvoreBinariaPesquisa<E>) (resultado.esquerda)).setPai(resultado.pai);
                if (resultado == resultado.pai.direita) {
                    resultado.pai.direita = resultado.esquerda;
                } else if (resultado == resultado.pai.esquerda) {
                    resultado.pai.esquerda = resultado.esquerda;
                }
            } else
            {
                resultado.valor = resultado.esquerda.valor;
                resultado.esquerda = null;
            }
        }
        else if (resultado.direita != null && resultado.esquerda != null) {
            while (resultado.direita != null && resultado.esquerda != null)
            {
                resultado = sucessor(resultado);
            }
            exclui(resultado);
            no.valor = resultado.valor;

        }
        return this;
    }
}
