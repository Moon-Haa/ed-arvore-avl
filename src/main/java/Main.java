
import utfpr.dainf.ct.ed.exemplo.ArvoreBinaria;

public class Main {
    
    public static void main(String[] args) {

        ArvoreBinaria<Integer> a = new ArvoreBinaria<>(8);

        ArvoreBinaria<Integer> e = a.insereEsquerda(new ArvoreBinaria<>(3));
        e.insereEsquerda(new ArvoreBinaria<>(1));
        ArvoreBinaria<Integer> d = e.insereDireita(new ArvoreBinaria<>(6));
        d.insereEsquerda(new ArvoreBinaria<>(4)).insereDireita(new ArvoreBinaria<>(5));
        d.insereDireita(new ArvoreBinaria<>(7));

        a.insereDireita(new ArvoreBinaria<>(10))
                .insereDireita(new ArvoreBinaria<>(14))
                .insereEsquerda(new ArvoreBinaria<>(13));

        System.out.println("PERCURSO RECURSIVO\nEmOrdem");
        a.visitaEmOrdem();

        System.out.println("\nPosOrdem");
        a.visitaPosOrdem();

        System.out.println("\nPreOrdem");
        a.visitaPreOrdem();

        System.out.println("\n\nPERCURSO ITERATIVO\nEmOrdem");
        ArvoreBinaria<Integer> no;
        while ((no = a.proximoEmOrdem()) != null) {
            System.out.print(" " + no.getValor()); }
        a.reinicia();

        System.out.println("\nPosOrdem");
        while ((no = a.proximoPosOrdem()) != null) {
            System.out.print(" " + no.getValor()); }
        a.reinicia();

        System.out.println("\nPreOrdem");
        while ((no = a.proximoPreOrdem()) != null) {
            System.out.print(" " + no.getValor()); }
        a.reinicia();

        System.out.println("\nEmNivel");
        while ((no = a.proximoEmNivel()) != null) {
            System.out.print(" " + no.getValor()); }
    }
}
